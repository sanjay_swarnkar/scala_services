import akka.actor.ActorSystem
import akka.actor.Props
import akka.io.IO
import spray.can.Http

//notification service

object Main  {
  def main(args:Array[String]){
 implicit val system = ActorSystem("HelloSystem")
  
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")
  IO(Http) ! Http.Bind(helloActor,interface="20.201.151.140",port=8091)
}
  
}
