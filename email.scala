import javax.mail._
import javax.mail.internet._
import java.util.Date
import java.util.Properties
import scala.collection.JavaConversions._

//sending email

class email(to: String,
                cc: String,
                bcc: String,
                from: String,
                subject: String,
                content: String,
                smtpHost: String,
                starttlsEnable:String,
                smtpAuth:String,
                passwrd:String,
                username:String) {
  //println("hii")
  
  var message: Message = null
   
  
 //println(message)
  message = createMessage
  //println(message)
  
  message.setFrom(new InternetAddress(from))
  setToCcBccRecipients

  message.setSentDate(new Date())
  message.setSubject(subject)
  message.setText(content)
  Transport.send(message)
 
 
 

  def createMessage: Message = {
    //println("hii")
    val properties = new Properties()
    properties.put("mail.smtp.host", smtpHost)
    properties.put("mail.smtp.starttls.enable",starttlsEnable )
    properties.put("mail.smtp.auth",smtpAuth)
    val session = Session.getDefaultInstance(properties,  new javax.mail.Authenticator() {
                    protected override def getPasswordAuthentication() =
                         new PasswordAuthentication(username, passwrd)
                    
                })/*null*/
    return new MimeMessage(session)
  }

  // throws AddressException, MessagingException
  def setToCcBccRecipients {
    // println("hii")
    setMessageRecipients(to, Message.RecipientType.TO)
    if (cc != null) {
      setMessageRecipients(cc, Message.RecipientType.CC)
    }
    if (bcc != null) {
      setMessageRecipients(bcc, Message.RecipientType.BCC)
    }
  }

  // throws AddressException, MessagingException
  def setMessageRecipients(recipient: String, recipientType: Message.RecipientType) {
    // had to do the asInstanceOf[...] call here to make scala happy
     //println("hii")
    val addressArray = buildInternetAddressArray(recipient).asInstanceOf[Array[Address]]
    if ((addressArray != null) && (addressArray.length > 0))
    {
      // println(addressArray)
      message.setRecipients(recipientType, addressArray)
    }
  }

  // throws AddressException
  def buildInternetAddressArray(address: String): Array[InternetAddress] = {
    // could test for a null or blank String but I'm letting parse just throw an exception
     //println("hii")
    return InternetAddress.parse(address)
  }

  
  

}