name := "Hello Test #1"
 
version := "0.13.8"
 
scalaVersion := "2.10.3"
 
 
libraryDependencies ++= Seq(
     "io.spray" % "spray-can" % "1.2.0",
     "io.spray" % "spray-routing" % "1.2.0",
     "com.azavea.geotrellis" %% "geotrellis" % "0.9.1",
      "javax.mail" % "mail" % "1.4",
      "postgresql" % "postgresql" % "9.1-901.jdbc4",
      "org.scala-tools.sbt" % "sbt-launch" % "0.7.2")